# kovs04-tymova-prace

Týmová práce pro LS 2020 4IT115
Aplikace pro správu knihovny

## Instalace

#### stažení projektu:
git clone https://gitlab.com/jhlubucek/knihovna-aplikace.git

#### instalace knihovny pro MySQL:
https://downloads.mysql.com/archives/c-j/
verze: 5.1.17
intelij: file > project structure > libraries (pridat .jar ze stažené složky)

#### nastevení připojení databáze
připojení k databázi se nastavuje v: knihovna-aplikace\src\logika\MysqlCon.java
ve složce db se nachází create script a schéma databáze