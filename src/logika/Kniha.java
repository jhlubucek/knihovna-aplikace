package logika;

/**
 * třída kniha
 */
public class Kniha {
    private int id;
    private String nazev;
    private String autor;
    private OblastZamereni oblastZamereni;

    /**
     * konstruktor
     * @param id
     * @param nazev
     * @param autor
     * @param oblastZamereni
     */
    public Kniha(int id, String nazev, String autor, OblastZamereni oblastZamereni) {
        this.id = id;
        this.nazev = nazev;
        this.autor = autor;
        this.oblastZamereni = oblastZamereni;
    }

    /**
     * konstruktor
     * @param id
     * @param nazev
     * @param autor
     * @param oblastId
     * @param oblastJmeno
     */
    public Kniha(int id, String nazev, String autor, int oblastId, String oblastJmeno) {
        this.id = id;
        this.nazev = nazev;
        this.autor = autor;
        this.oblastZamereni = new OblastZamereni(oblastId, oblastJmeno);
    }

//    @Override
//    public String toString() {
//        return String.format("%d %s %s %d %s", id, nazev, autor, oblastZamereni.getId(), oblastZamereni.getNazev());
//    }

    public int getId() {
        return id;
    }

    public String getIdString() {
        return String.valueOf(id);
    }

    public String getNazev() {
        return nazev;
    }

    public String getAutor() {
        return autor;
    }

    public OblastZamereni getOblastZamereni() {
        return oblastZamereni;
    }

    public String getOblastNazev() {
        return oblastZamereni.getNazev();
    }

    public int getOblastId() {
        return oblastZamereni.getId();
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setOblastZamereni(OblastZamereni oblastZamereni) {
        this.oblastZamereni = oblastZamereni;
    }

    @Override
    public String toString() {
        return String.format("%s :: %s (%d)", nazev, autor, id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Kniha)) return false;
        Kniha that = (Kniha) o;
        return getNazev().equals(that.getNazev())
                && this.getId() == (that.getId());
    }
}
